#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):

    def test_read_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_read_2(self):  
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_read_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")    
    
    def test_read_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_read_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")       

    def test_read_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")  

    def test_read_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")   
    
    def test_read_8(self):
        r = StringIO("A Anehiem Move Honolulu\nB Honolulu Hold\nC Bogota Support B\nD Bogota Support A\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")   
    
    def test_read_9(self):
        r = StringIO("A Anehiem Move Honolulu\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Honolulu\n")   
    
    def test_read_10(self):
        r = StringIO("A Austin Move LosAngeles\nB LosAngeles Move Seattle\nC Seattle Move Cambridge\n")
        w = StringIO("")
        i = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A LosAngeles\nB Seattle\nC Cambridge\n")

if __name__ == "__main__":  # pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
